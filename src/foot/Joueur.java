/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foot;

import java.time.LocalDate;

/**
 *
 * @author GARRETA
 */
public class Joueur extends Humain{
    private int numeroMaillot;
    private Equipe Jouepour;
    
    public Joueur(String paramNom,String paramPrenom,LocalDate paramDateNaissance,int paramnumeroMaillot,Equipe paramJouepour) throws Exception{
         super (paramNom,paramPrenom,paramDateNaissance);
         this.numeroMaillot=paramnumeroMaillot;
         if(numeroMaillot>0){
            throw new Exception("le numéro de maillot n'est pas valide"); 
        }
        
    }
    public int setnumeroMaillot(int paramnumeroMaillot){
    this.numeroMaillot=paramnumeroMaillot;
}
    
    public int getnumeroMaillot(){
    return numeroMaillot;
}
    public Equipe getEquipe(){
    return Jouepour;
}
    
    public String donneTexte(){
        return ""+getPrenom()+" "+getNom()+" : joueur "+Jouepour+" numéro : "+numeroMaillot;
    }
}
