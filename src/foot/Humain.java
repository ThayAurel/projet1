/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foot;
import java.util.Date;
import java.util.Calendar;
import  java.lang.System;
import java.time.LocalDate;


/**
 *
 * @author GARRETA
 */
public class Humain {
    private String nom;
    private String prenom;
    private LocalDate DateNaissance;
    
    public Humain(String paramNom,String paramPrenom,LocalDate paramDateNaissance)throws Exception{
        if(paramNom.equals("")){
            throw new Exception("le nom vide"); 
        }
        
        if(paramPrenom.equals("")){
            throw new Exception("le prenom vide"); 
        }
       
        if(paramDateNaissance.equals("")){
            throw new Exception("l DateNaissance vide"); 
        }
       
        
        this.nom=paramNom;
        this.prenom=paramPrenom;
        this.DateNaissance=paramDateNaissance;
    }
    
    public String getNom(){
        return nom;
    }
    
    public String getPrenom(){
        return prenom;
    }
    public LocalDate getDateNaissance(){
        return DateNaissance;
    }
    
    public void setNom(String paramNom){
        this.nom=paramNom;
    }
    
    public void setPrenom(String paramPrenom){
        this.prenom=paramPrenom;
    }
    
     public void setDateNaissance(LocalDate paramDateNaissance){
        this.DateNaissance=paramDateNaissance;
    }
     
    public String donneTexte(){
        return " "+prenom+" "+nom+" "+"né le "+DateNaissance;
    }
    
}
