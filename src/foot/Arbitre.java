/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foot;

import java.time.LocalDate;

/**
 *
 * @author GARRETA
 */
public class Arbitre extends Humain {
    private String pays;
    
    public Arbitre(String paramNom,String paramPrenom,LocalDate paramDateNaissance,String paramPays) throws Exception{
        super (paramNom,paramPrenom,paramDateNaissance);
        if(paramPays.equals("")){
            throw new Exception("le pays est vide"); 
        }
        this.pays=paramPays;
    }
    public String getPays(){
        return pays;
    }
    public String donneTexte(){
        return ""+getPrenom()+" "+getNom()+" : arbitre "+pays;
    }
    
}
