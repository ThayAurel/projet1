/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foot;
import java.util.List; // import just the List interface
import java.util.ArrayList;

/**
 *
 * @author GARRETA
 */
public class Equipe {
    
    ArrayList<Joueur> listeJoueurs = new ArrayList<>();
    private String pays;
    private Sélectionneur estentraine;
    public Equipe(String parampays/*,Sélectionneur paramestentraine*/){
        this.pays=parampays;
        /*this.estentraine=paramestentraine;*/
        
    }
    
    public void AjouterJoueur(Joueur paramJoueur){
        listeJoueurs.add(paramJoueur);
    }
    
    public String getPays(){
        return pays;
    }
    
    public String Equipe(){
        return pays;
    }
    
    public Sélectionneur GetEstEntrainée(){
        return estentraine;
    }
    
    public String avoirnomselectionneur(){
     
        return estentraine.getNom()+" "+estentraine.getPrenom();
    }
    public void setSélectionneur(Sélectionneur selectionneur){
        this.estentraine=selectionneur;
    }
   
    public ArrayList<Joueur> GetListJoueur(){
        return listeJoueurs;
    }
    public String AfficherListeJoueurs(){
        String str="\n liste des joueurs : ";
        for (int i = 0; i < listeJoueurs.size(); i++) {
            str=str+"\n"+listeJoueurs.get(i).getNom()+" "+listeJoueurs.get(i).getPrenom()+" numéro:"+listeJoueurs.get(i).getnumeroMaillot();
        }
        return str;
    }
    
    public String donneTexte(){
        return "Equipe : "+pays+"\n Sélectionneur : "+avoirnomselectionneur()+" "+
                AfficherListeJoueurs();
    }
}
