/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foot;

import java.time.LocalDate;
import java.util.HashSet;

/**
 *
 * @author GARRETA
 */
public class Sélectionneur extends Humain {
    Equipe entraine;
    public Sélectionneur(String paramNom,String paramPrenom,LocalDate paramDateNaissance,Equipe paramequipe) throws Exception{
        super (paramNom,paramPrenom,paramDateNaissance);
        this.entraine=paramequipe;
        this.entraine.setSélectionneur(this);
        
    }
   public Equipe getEquipe(){
        return entraine;
    }
public String donneTexte(){
        return ""+getPrenom()+" "+getNom()+" : Sélectionneur ";
    }
}
